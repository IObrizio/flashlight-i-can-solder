# **Flashlight I can solder**

This is a basic circuit to show you how to turn on a LED and use
it as a flashlight.
You will learn to use some tools like soldering iron and cutting
pliers, also to identify electronics symbols.
It’s an entry DIY kit so previous knowledge is no need.


Printed circuit board files are shared

Schematic and Board.
Autodesk Eagle 9.5.2

https://twitter.com/IObrizio

This project won the GOLD " I can solder kit 2019" award by PCBway
https://www.pcbway.com/project/shareproject/Flashlight___I_can_Solder___STEAM.html